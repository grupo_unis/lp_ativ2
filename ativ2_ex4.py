#Implementar uma função que retorne verdadeiro se o número for primo (falso caso contrário). Testar de 1 a 100.

def VRprimo(primo):
    for i in range(2, primo+1):
        if i != primo:
            i = primo % i
            if i == 0:
                return "Falso"
            else:            
                return 'Verdadeiro'

for i in range(1,101):
    print(i,"=>", VRprimo(i))
print("FIM")

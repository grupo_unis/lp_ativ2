#Elaborar um programa que lê 3 valores a,b,c e verifica se eles formam ou não um triângulo. 
# Supor que os valores lidos são inteiros e positivos. Caso os valores formem um triângulo, calcular e escrever a área deste triângulo. 
#Se não formam triângulo escrever os valores lidos. (Se a > b + c não formam triângulo algum, se a é o maior).

'''
Formula do triangulo sera A = B X C / 2
-- A=base ×altura/2 --
'''
A = int(input('Digite o valor de A: \n'))
B = int(input('Digite o valor de B: \n'))
C = int(input('Digite o valor de C: \n'))

if (A > B + C):
    print("Nao formam triangulo algum, se A e maior")
else:
    area = (B * C // 2)
    print(area)